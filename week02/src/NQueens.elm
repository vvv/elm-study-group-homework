module NQueens exposing
    ( Field
    , enumerated
    , fits
    , main
    , posBeatenBy
    , queens
    , tryPlace
    , tryPosition
    )

import Array exposing (Array)
import Html exposing (text)


type alias Field =
    List Int


tryPlace : Int -> Field -> List Field
tryPlace left field =
    case left of
        0 ->
            [field]

        _ ->
            let
                positions =
                    List.range 1 8
            in
            List.concatMap (tryPosition left field) positions


tryPosition : Int -> Field -> Int -> List Field
tryPosition left field pos =
    case fits pos field of
        Just newField ->
            tryPlace (left - 1) newField

        Nothing ->
            []


enumerated : Field -> List ( Int, Int )
enumerated xs =
    let
        go ys i =
            case ys of
                [] ->
                    []

                z :: zs ->
                    ( i, z ) :: go zs (i + 1)
    in
    go xs 1


fits : Int -> Field -> Maybe Field
fits pos field =
    case
        List.any
            (posBeatenBy
                ( List.length field + 1, pos )
            )
            (enumerated field)
    of
        False ->
            Just (field ++ [ pos ])

        True ->
            Nothing


posBeatenBy : ( Int, Int ) -> ( Int, Int ) -> Bool
posBeatenBy ( x1, y1 ) ( x2, y2 ) =
    if x1 == x2 || y1 == y2 || (abs (x1 - x2) == abs (y1 - y2)) then
        True

    else
        False


queens : List (List Int)
queens =
    tryPlace 8 []


main =
    text <| String.fromInt <| List.length queens
